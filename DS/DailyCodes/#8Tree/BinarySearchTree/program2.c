
	#include<stdio.h>
	#include<stdlib.h>

	typedef struct Node{
	
		int data;
		struct Node *left,*right;

	}Node;

	Node *root = NULL;

	Node *createNode(int data) {
	
	
		Node *newNode = (Node*)malloc(sizeof(Node));
		newNode->left = newNode->right = NULL;
		newNode->data = data;

		return newNode;
	}

	Node* createTree(int data,Node *root) {
	
		if(root == NULL)
			return createNode(data);
		
		if(data < root->data)
			root->left = createTree(data,root->left);
		else
			root->right = createTree(data,root->right);
			
		return root;
	}

	void preOrder(Node *root) {
	
		if(root != NULL) {
		
			printf("%d ",root->data);
			preOrder(root->left);
			preOrder(root->right);
		}

	}

	int minVal(Node *root) {
	
		int min = root->data;

		while(root->left != NULL) {
		
			min = root->left->data;
			root = root->left;
		}

		return min;
	}

	Node *deleteNode(int data,Node *root) {
	
		if(root == NULL) 
			return NULL;

		if(data < root->data)
			root->left =  deleteNode(data,root->left);

		else if(data > root->data) 
			root->right =  deleteNode(data,root->right);
		
		else {

			if(root->left == NULL)
				return root->right;
			else if(root->right == NULL)
				return root->left;
			root->data = minVal(root->right);
			root->right = deleteNode(root->data,root->right);
		}

		return root;
	}

	void main() {

		int arr[] = {50,100,10,20,30};
		
		for(int i=0;i<5;i++)
			root = createTree(arr[i],root);

		preOrder(root);
		printf("\n");
		root = deleteNode(10,root);
		preOrder(root);
		printf("\n");

	}


