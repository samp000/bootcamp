
	#include<stdio.h>
	#include<stdlib.h>
	#include<stdbool.h>

	typedef struct TreeNode {
	
		int data;
		struct TreeNode* left;
		struct TreeNode* right;
	}TreeNode;

	TreeNode* createNode(int data){
	
		TreeNode *newNode = (TreeNode*)malloc(sizeof(TreeNode));
		newNode->data = data;
		newNode->left = newNode->right = NULL;

		return newNode;
	}

	TreeNode* createBST(TreeNode *root,int data) {
	
		if(root == NULL){
			root = createNode(data);
			return root;
		}

		if(data < root->data)
			root->left = createBST(root->left,data);
		else	
			root->right = createBST(root->right,data);

		return root;
	}

	void preOrder(TreeNode *root) {

		if(root == NULL)
			return;

		printf("%d ",root->data);
		preOrder(root->left);
		preOrder(root->right);
	}
	
	void inOrder(TreeNode *root) {

		if(root == NULL)
			return;

		inOrder(root->left);
		printf("%d ",root->data);
		inOrder(root->right);
	}
	
	void postOrder(TreeNode *root) {

		if(root == NULL)
			return;

		inOrder(root->left);
		inOrder(root->right);
		printf("%d ",root->data);
	}

	void printTree(TreeNode *root) {
	
		printf("\nPreOrder: ");
		preOrder(root);
		printf("\nInOrder: ");
		inOrder(root);
		printf("\nPostOrder:");
		postOrder(root);
		printf("\n");
	} 

	bool searchInBST1(TreeNode *root,int data) {		//recursive
	
		if(root == NULL)
			return false;
		if(root->data == data)
			return true;
		if(root->data > data)
			return searchInBST1(root->left,data);

		return searchInBST1(root->right,data);
	} 

	bool searchInBST2(TreeNode *root,int data) {		//iterative

		TreeNode *tmp = root;

		while(root != NULL){
		
			if(tmp->data == data)
				return true;
			if(tmp->data > data)
				tmp = tmp->left;
			else
				tmp = tmp->right;
		}

		return false;
	}


	void main() {
	
		TreeNode *root = NULL;

		int n;
		printf("Enter no of nodes in BST:");
		scanf("%d",&n);

		for(int i=1;i<=n;i++) {
			int data;
		
			printf("Enter data:");
			scanf("%d",&data);

			root = createBST(root,data);
		}

		printTree(root);

		int data;
		printf("Enter data to search:");
		scanf("%d",&data);

		//search in BST
		if(searchInBST2(root,data))
			printf("Found\n");
		else
			printf("Not Found\n");
	}


