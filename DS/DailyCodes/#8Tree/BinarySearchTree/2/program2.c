
	#include<stdio.h>
	#include<stdlib.h>
	#include<stdbool.h>

	typedef struct TreeNode {
	
		int data;
		struct TreeNode* left;
		struct TreeNode* right;
	}TreeNode;

	TreeNode* createNode(int data){
	
		TreeNode *newNode = (TreeNode*)malloc(sizeof(TreeNode));
		newNode->data = data;
		newNode->left = newNode->right = NULL;

		return newNode;
	}

	TreeNode* createBST(TreeNode *root,int data) {
	
		if(root == NULL){
			root = createNode(data);
			return root;
		}

		if(data < root->data)
			root->left = createBST(root->left,data);
		else	
			root->right = createBST(root->right,data);

		return root;
	}

	void preOrder(TreeNode *root) {

		if(root == NULL)
			return;

		printf("%d ",root->data);
		preOrder(root->left);
		preOrder(root->right);
	}
	
	void inOrder(TreeNode *root) {

		if(root == NULL)
			return;

		inOrder(root->left);
		printf("%d ",root->data);
		inOrder(root->right);
	}
	
	void postOrder(TreeNode *root) {

		if(root == NULL)
			return;

		inOrder(root->left);
		inOrder(root->right);
		printf("%d ",root->data);
	}

	void printTree(TreeNode *root) {

		printf("-------------------------------------\n");

		printf("\nPreOrder: ");
		preOrder(root);
		printf("\nInOrder: ");
		inOrder(root);
		printf("\nPostOrder:");
		postOrder(root);
		printf("\n");
		
		printf("-------------------------------------\n");
	} 

	bool searchInBST1(TreeNode *root,int data) {		//recursive
	
		if(root == NULL)
			return false;
		if(root->data == data)
			return true;
		if(root->data > data)
			return searchInBST1(root->left,data);

		return searchInBST1(root->right,data);
	} 

	bool searchInBST2(TreeNode *root,int data) {		//iterative

		TreeNode *tmp = root;

		while(root != NULL){
		
			if(tmp->data == data)
				return true;
			if(tmp->data > data)
				tmp = tmp->left;
			else
				tmp = tmp->right;
		}

		return false;
	}

	TreeNode* insertNode(TreeNode* root,int data) {
	
		return createBST(root,data);
	}

	int minInBST(TreeNode* root) {
	
		TreeNode* tmp = root;

		while(tmp->left != NULL)
			tmp = tmp->left;

		return tmp->data;
	}
	
	int maxInBST(TreeNode* root) {
	
		TreeNode* tmp = root;

		while(tmp->right != NULL)
			tmp = tmp->right;

		return tmp->data;
	}

	TreeNode* deleteInBST(TreeNode *root,int data) {
	
		if(root == NULL)
			return root;

		if(root->data > data) {
		
			root->left = deleteInBST(root->left,data);
		
		}else if(root->data < data) {
			
			root->right = deleteInBST(root->right,data);
		
		}else {
		
			
			if(root->left == NULL && root->right == NULL) {

				free(root);
				return NULL;

			}else if(root->left == NULL && root->right != NULL) {
				
				TreeNode *ret = root->right;
				free(root);
				return ret;
				
			}else if(root->left != NULL && root->right == NULL) {
				
				TreeNode *ret = root->left;
				free(root);
				return ret;
			}else {

				int max = maxInBST(root->left);
				root->data = max;
				printf("max:%d\n",max);
				root->left = deleteInBST(root,max);
				return root;
			}
		}
			
		return root;
	}


	void main() {
	
		TreeNode *root = NULL;

		int ch;

		while(1) {
		
			printf("1.Insert In Node\n");
			printf("2.Print Tree\n");
			printf("3.Search In Tree\n");
			printf("4.Min In Tree\n");
			printf("5.Max In Tree\n");
			printf("6.Delete From Tree\n");
			printf("7.Exit\n\n");

			printf("Select option:");
			scanf("%d",&ch);

			switch(ch) {
			
				case 1:{
					int data;
					printf("Enter data to insert:");
					scanf("%d",&data);
					root = insertNode(root,data);
				       }
				       break;
				
				case 2:
				       printTree(root);
				       break;

				case 3:{
					       
					printf("-------------------------------------\n");
					int data;
					printf("Enter data to search:");
					scanf("%d",&data);

					if(searchInBST2(root,data))
						printf("Found\n");
					else
						printf("Not Found\n");
					printf("-------------------------------------\n");
					
				       }
				       
				       	break;
				
				case 4:{
					printf("-------------------------------------\n");
					printf("Minimum in tree:%d\n",minInBST(root));
					printf("-------------------------------------\n");
				       }
				       break;
				
				case 5:{
					printf("-------------------------------------\n");
					printf("Maximum in tree:%d\n",maxInBST(root));
					printf("-------------------------------------\n");
				       }
				       break;

				case 6:{
					printf("-------------------------------------\n");
					int data;
					printf("Enter data to delete:");
					scanf("%d",&data);

					root = deleteInBST(root,data);
					printf("-------------------------------------\n");
				       }
				       break;
				case 7:
					exit(0);
					break;

				case 10:
					{
						printf("----------------------------------------------\n");
						printf("Root address:%p\n",root);
						printf("----------------------------------------------\n");
					}
					break;

			}
		}

	}


