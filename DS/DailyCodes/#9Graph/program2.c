
	#include<stdio.h>
	#include<stdlib.h>

	typedef struct Node {
	
		int data;
		struct Node *next;
	
	}Node;


	void printList(Node *list[],int v) {
	
		for(int i=0;i<v;i++) {
		
			printf("%d-> ",i);

			Node *head = list[i];

			
			while(head != NULL) {
			
				printf("%d ",head->data);
				head = head->next;
			
			}


			printf("\n");
		}
	}

	void initList(Node *list[],int v) {
	
		for(int i=0;i<v;i++) {
		
			list[i] = NULL;
		}
	}

	void setEdge(Node **list,int end) {
	
		Node *head = *list;
			
		Node *newNode = (Node*)malloc(sizeof(Node));
		newNode->data = end;
		newNode->next = NULL;

		if(head == NULL) {
		
			*list = newNode;
		}else {
		
			while(head->next != NULL) 
				head = head->next;

			head->next = newNode;
		}
	}

	void connectEdges(Node *list[],int v,int e) {
	
		int start,end;

		for(int i=0;i<e;i++) {
		
			printf("Start:");
			scanf("%d",&start);
			printf("End:");
			scanf("%d",&end);

			setEdge(&list[start],end);
		}
	}

	void main() {
	
		int v,e;
		printf("Enter no of vertices:");
		scanf("%d",&v);
		printf("Enter no of edges:");
		scanf("%d",&e);
	
		
		Node* list[v];

		initList(list,v);
		printList(list,v);

		connectEdges(list,v,e);	
		
		printList(list,v);
	}
