
	//weighted graph using Matrix

	#include<stdio.h>	

	void main() {

		int nodes = 4;

		int graph[][4] = {
			{0,4,0,0},
			{4,0,5,0},
			{0,5,0,6},
			{0,0,6,0}};

		for(int i=0;i<nodes;i++) {
		
			for(int j=0;j<nodes;j++) {
			
				if(graph[i][j] != 0) {
				
					printf("%d -> %d = %d\n",i+1,j+1,graph[i][j]);
				}
			}
		}
		
	}
