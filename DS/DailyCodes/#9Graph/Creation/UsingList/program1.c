
	#include<stdio.h>
	#include<stdlib.h>

	typedef struct Node{
		
		int data;
		struct Node *next;

	}Node;

	Node* createNode(int data) {
	
		Node* tmp = (Node*)malloc(sizeof(Node));
		tmp->data = data;
		tmp->next = NULL;
	}

	void addNode(Node** head,int data) {
		
		Node* tmp  = createNode(data);
		tmp->next = *head;
		*head = tmp;
	}

	void getC(int i,char* ch) {
	
		switch(i) {
		
			case 0: *ch = 'A';
				break;
			case 1: *ch = 'B';
				break;
			case 2: *ch = 'C';
				break;
			case 3: *ch = 'D';
				break;
		}
	}

	void printList(Node* graph[],int nodes) {
	
		for(int i=0;i<nodes;i++) {
		
			Node *tmp = graph[i];
			
			// 's' represents source and 'd' represents destination	
			char s, d;

			getC(i,&s);

			while(tmp != NULL) {
			
				getC(tmp->data,&d);
				printf("%c -> %c\n",s,d);
				tmp = tmp->next;
			}
		}
	}


	void main() {
	
		int nodes = 4;

		Node* graph[4] = {NULL,NULL,NULL,NULL};

		//edges from A
		addNode(&graph[0],1);

		//edges from B
		addNode(&graph[1],0);
		addNode(&graph[1],2);

		//edges from C
		addNode(&graph[2],1);
		addNode(&graph[2],3);

		//edges from D
		addNode(&graph[3],2);

		printList(graph,nodes);
	}
	
