
	#include<stdio.h>

	void printMat(int v, int mat[][v]) {
	
		for(int i=0;i<v;i++) {
		
			for(int j=0;j<v;j++) {
			
				printf("%d ",mat[i][j]);
			}

			printf("\n");
		}
	}

	void setMat(int v,int mat[][v]) {
	
		for(int i=0;i<v;i++) {
		
			for(int j=0;j<v;j++) {
			
				mat[i][j] = 0;
			}
		}
	}
	
	void setEdge(int start,int end,int v,int mat[][v]) {

		mat[start][end] = 1;
	}

	void connect(int v,int e,int mat[][v]) {
	
		int start,end;
		
		for(int i=0;i<e;i++) {
		
			printf("Enter start:");
			scanf("%d",&start);
			
			printf("Enter end:");
			scanf("%d",&end);

			setEdge(start,end,v,mat);
		}
	}

	void main() {
	
		int v,e;
		printf("Enter no of vertices:");
		scanf("%d",&v);
		printf("Enter no of edges:");
		scanf("%d",&e);
		
		int mat[v][v];

		setMat(v,mat);
		printMat(v,mat);

		connect(v,e,mat);
		printMat(v,mat);

	}
