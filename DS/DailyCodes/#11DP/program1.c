
	#include<stdio.h>

	static int st=0;
		
	int dp[100];

	int fib(int n) {
	

		if(n <= 1)
			return n;

		printf("Stack:%d\n",++st);

		if(dp[n] != -1)
			return dp[n];
	

		dp[n] = fib(n-1) + fib(n-2);

		return dp[n];
	}

	void main() {
	
		int n;
		
		printf("Enter n:");
		scanf("%d",&n);
	
		for(int i=0;i<n+1;i++) {
		
			dp[i] = -1;
		}


		printf("Fib of n:%d\n",fib(n));
	}
