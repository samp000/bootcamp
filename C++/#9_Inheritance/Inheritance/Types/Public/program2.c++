
	#include<iostream>

	class Parent {
	
		int x=10;

		public:
		Parent() {
		
			std::cout << "Parent constructor" << std::endl;
		}

		void getData(){
			std::cout << "parent x: "<<x << std::endl;
		}
	};

	class Child:public Parent {
		
		int x=10;
	
		public:
			Child() {
			
				std::cout << "Child constructor" << std::endl;
			}
		
		
		void getData(){
			std::cout << "child x: "<<x << std::endl;
		
		}
	};


	int main() {

		
		Parent *obj = new Child();
		//getData() will always called of Parent, this is based on Parent (decided at runtime)
		obj->getData();


		return 0;
	}
