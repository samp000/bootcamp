
	#include<iostream>

	class Parent {
	
		int x=10;

		public:
		Parent() {
		
			std::cout << "Parent constructor" << std::endl;
		}

		void getData(){
			std::cout << "parent x: "<<x << std::endl;
		}
	};

	class Child:public Parent {
		
		int x=10;
	
		public:
			Child() {
			
				std::cout << "Child constructor" << std::endl;
			}
		
		
		void getData(){
			//Parent::getData();		1st
			std::cout << "child x: "<<x << std::endl;
		
		}
	};


	int main() {

		Child obj;
		obj.getData();

		//2nd
		//we are typecasting child obj to Parent and then calling parent method
		//dont use this because this calls to copy constructor
		(Parent(obj)).getData();

		//3rd
		obj.Parent::getData();

		//4th
		Parent &obj2 = static_cast<Parent&>(obj);
		obj2.getData();

		return 0;
	}
