
	#include<iostream>	

	class Parent{
	
		int x=10;

		public:

		//this is not a member function
		friend std::ostream& operator<<(std::ostream& out ,Parent& obj) {
		
			out<< "In parent" << std::endl;
			out << obj.x;

			return out;
		}

	};

	class Child:public Parent{
	

	};

	int main() {
	
		Child obj;

		//this call goes like =>
		//operator<<(cout,obj)
		//this obj is typecasted to parent reference(possible because of inheritance)
		//
		
		std::cout << obj << std::endl;
		std::cout << (Parent&)obj << std::endl;	//same as above

		return 0;
	}

	
