
	#include<iostream>

	class Parent {

		int x = 10;
		int y = 20;

		public:

		Parent() {
			std::cout << "Parent constructor" << std::endl;
			std::cout << "In no-arg parent: " << this  << std::endl;
		}
		
		Parent(int x,int y) {
		
			this->x = x;
			this->y = y;
			
			std::cout << "Parent constructor para" << std::endl;
			std::cout << "in para parent: "<<this  << std::endl;
		}

		void getData() {
		
		
			std::cout << "x: "<< x <<std::endl;
			std::cout << "y: "<< y <<std::endl;
		}
		

	};

	class Child:public Parent {
	
		int z=30;

		public:

		Child(int x,int y,int z):Parent(x,y){

			//Parent(x,y);
			std::cout << "Child constructor" << std::endl;
			std::cout << "In child: " << this  << std::endl;
		}
		
	};

	int main() {
	
		Child obj(40,50,60);
		obj.getData();

		return 0;
	}
