
	#include<iostream>

	class Parent {
	
		int x=10;

		protected:
			int y=20;

		public:
			int z = 30;

			void getData() {
			
				std::cout << "In getData" << std::endl;
			}
	};

	class Child:public Parent {
	
		
		using Parent::getData;		//reduces scope of getData() to private
							
		public:
			using Parent::y;		//y scope increased to public
			
	};

	int main() {
	
		Child obj;
		std::cout << obj.y << obj.z << std::endl;
		//obj.getData();		error
		return 0;
	}
