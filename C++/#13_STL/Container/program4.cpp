
	#include<iostream>
	#include<vector>

	int main() {
	
		std::vector<int> vobj;

		vobj.push_back(10);
		vobj.push_back(20);
		vobj.push_back(30);
		vobj.push_back(40);
		vobj.push_back(50);

		//prints in sequence from begining
		for(auto itr = vobj.begin();itr< vobj.end();itr++)
			std::cout << *itr << std::endl;
		
		//prints in sequence from begining in reverse
		for(auto itr = vobj.rbegin();itr< vobj.rend();itr++)
			std::cout << *itr << std::endl;
		
		/*
		//we cannot change values using... "cbegin()"
		for(auto itr = vobj.cbegin();itr< vobj.cend();itr++) {
		
			*itr = 100;		//error
			std::cout << *itr << std::endl;

		}*/
		
		//this combines "cbegins()" and "rbegins()"
		for(auto itr = vobj.crbegin();itr< vobj.crend();itr++)
			std::cout << *itr << std::endl;

		return 0;
	}

