
	#include<iostream>
	#include<list> 

	void printList(std::list<int>,std::string);
	void printSize(std::list<int>,std::string);
	bool condition(const int&);
	
	int main() {

		std::list<int> lst;
		std::list<int>::iterator itr;

		lst.push_back(10);
		lst.push_back(20);
		lst.push_back(30);
		lst.push_back(40);
		lst.push_back(50);
		lst.push_front(5);

		//printing list
		printList(lst,"lst");
		printSize(lst,"lst");
		
		lst.pop_front();
		
		printList(lst,"lst");
		printSize(lst,"lst");

		std::cout << "\n---------------access element------------------"<<std::endl;
		std::cout << "front: "<<lst.front()<<std::endl;
		std::cout << "back: "<<lst.back()<<std::endl;
		
		std::list<int> lst1;
		
		std::cout << "\n---------------assign------------------"<<std::endl;
		lst1.assign(10,100);
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n---------------emplace------------------"<<std::endl;
		//insert element at given position(by iterator) 
		lst.emplace(lst.end(),10000);
		printList(lst,"lst");
		printSize(lst,"lst");
		
		std::cout << "\n---------------emplace_front------------------"<<std::endl;
		//inserts element at front
		lst.emplace_front(1111);
		printList(lst,"lst");
		printSize(lst,"lst");


		std::cout << "\n---------------insert------------------"<<std::endl;
		//inserts 500 at current position of iterator
		itr = lst.begin();
		itr++;
		itr = lst.insert(itr,500);	
		printList(lst,"lst");
		printSize(lst,"lst");
		
		//inserts 500, 5 times from current position of interator
		lst.insert(itr,5,500);
		printList(lst,"lst");
		printSize(lst,"lst");

		itr = lst.begin();
		advance(itr,2);

		std::cout << "\n---------------erase------------------"<<std::endl;
		//erases element at current iterator pos
		lst.erase(itr); //(500 will be removed)
		printList(lst,"lst");
		printSize(lst,"lst");
		

		std::cout << "\n---------------swap------------------"<<std::endl;
		//swaps content of "lst" with "lst1"
		lst.swap(lst1);
		printList(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst,"lst");
		printSize(lst1,"lst1");

		
		std::cout << "\n---------------resize------------------"<<std::endl;
		//resizes list to given size
		lst1.resize(5);
		printList(lst1,"v1");
		printSize(lst1,"v1");
		

		std::cout << "\n---------------clear------------------"<<std::endl;
		//removes all values from list "lst1"
		lst1.clear();
		printList(lst1,"lst1");
		printSize(lst1,"lst1");

		lst1.push_back(10);
		lst1.push_back(5);
		lst1.push_back(3);
		lst1.push_back(2);
		
		lst.resize(5);
		lst.pop_front();
		lst.pop_front();
		lst.pop_front();
		lst.push_back(500);
		lst.push_front(2);
		lst.push_back(50);
		
		
		std::cout << "\n---------------------------------"<<std::endl;
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n---------------sort------------------"<<std::endl;
		lst1.sort();
		lst.sort();
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n---------------merge------------------"<<std::endl;
		lst.merge(lst1);
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n---------------reverse------------------"<<std::endl;
		lst.reverse();
		printList(lst,"lst");
		printSize(lst,"lst");

		lst1.push_back(60);
		lst1.push_back(70);
		lst1.push_back(25);
		lst1.push_back(30);
		lst1.push_back(35);
		lst1.push_back(80);

		std::cout << "\n---------------------------------"<<std::endl;
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n------------------splice(1,2)---------------"<<std::endl;
		itr = lst.begin();
		++itr;	//itr at 2nd position(0->1)
		//moves lst1 to lst at "itr" standing(makes lst1 empty)
		lst.splice(itr,lst1);
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n------------------splice(1,2,3)---------------"<<std::endl;
		itr = lst.begin();
		advance(itr,3);	//moves 3 palces next
		//moves element at "itr" to lst1
		lst1.splice(lst1.begin(),lst,itr);
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n------------------splice(1,2,3,4)---------------"<<std::endl;
		itr = lst.begin();//intializes iterator
		advance(itr,3);	//moves 3 palces next
		//moves lst to lst1's starting position from 3rd element to end of list
		lst1.splice(lst1.begin(),lst,itr,lst.end());
		printList(lst,"lst");
		printSize(lst,"lst");
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n----------------remove-----------------"<<std::endl;
		//removes 500 from lst
		lst.remove(500);
		printList(lst,"lst");
		printSize(lst,"lst");
			
		std::cout << "\n----------------remove_if-----------------"<<std::endl;
		
		//removes_if element is div by 5 && not div by 10(this is checked by consdition())
		lst1.remove_if(condition);
		printList(lst1,"lst1");
		printSize(lst1,"lst1");
		
		std::cout << "\n----------------unique-----------------"<<std::endl;
	
		//removes duplicate values	
		lst1.unique();
		printList(lst1,"lst1");
		printSize(lst1,"lst1");

			
		return 0;

	}

	bool condition(const int& val) {	
		return (val%5==0 && val%10 != 0);
	}


	
	void printList(std::list<int> l,std::string lName) {

		std::list<int>::iterator itr;
		
		std::cout << "list "<< lName << ": ";

		for(itr = l.begin();itr!=l.end();itr++) 
			std::cout << *itr <<" ";

		if(l.empty())
			std::cout << "empty";
		
		std::cout << std::endl;
	}

	void printSize(std::list<int> l,std::string lName) {
		std::cout << "Size " << lName+": "<<l.size() <<std::endl;
	}


	

