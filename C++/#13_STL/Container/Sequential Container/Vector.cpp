
	#include<iostream>
	#include<vector> 

	void printVector(std::vector<int>,std::string);
	void printSize(std::vector<int>,std::string);
	void printCapacity(std::vector<int>,std::string);

	int main() {

		std::vector<int> v;
		std::vector<int>::iterator itr;

		v.push_back(10);
		v.push_back(20);
		v.push_back(30);
		v.push_back(40);
		v.push_back(50);
		v.push_back(60);
		v.push_back(70);
		v.push_back(80);

		//printing vector
		printVector(v,"v");
		printSize(v,"v");
		printCapacity(v,"v");

		std::cout << "\n---------------accessing data------------------"<<std::endl;
		std::cout << "v[0]: " <<v[0] << std::endl;
		std::cout << "at(1): " <<v.at(1) << std::endl;
		std::cout << "front: " <<v.front() << std::endl;
		std::cout << "back: " <<v.back() << std::endl;

		//data() returns pointer of first element
		std::cout << "data()+4: " <<*(v.data()+4) << std::endl;
		
		std::cout << "\n---------------insert------------------"<<std::endl;
		//inserts 200 at current position of iterator
		itr = v.begin()+2;
		itr = v.insert(itr,200);	
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		
		//inserts 200, 5 times from current position of interator
		v.insert(itr,5,200);
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;

		itr = v.begin();

		std::cout << "\n---------------erase------------------"<<std::endl;
		//erases element at current iterator pos
		v.erase(itr+2); //(200 will be removed)
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		
		//erases element from current iterator pos"1st para" to "2nd para"
		v.erase(itr+2,itr+7); // 5 values will be removed
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		
		std::vector<int> v1 = {100,200,300};

		std::cout << "\n---------------swap------------------"<<std::endl;
		//swaps content of "v" with "v1"
		v.swap(v1);
		printVector(v,"v");
		printVector(v1,"v1");
		printSize(v,"v");
		printSize(v1,"v1");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		std::cout << "Capacity vector v1:"<< v1.capacity() << std::endl;
		
		std::cout << "\n---------------resize------------------"<<std::endl;
		//resizes vector to given size
		v1.resize(5);
		printVector(v1,"v1");
		printSize(v1,"v1");
		std::cout << "Capacity vector v1:"<< v1.capacity() << std::endl;
		

		std::cout << "\n---------------clear------------------"<<std::endl;
		//removes all values from vector "v1"
		v1.clear();
		printVector(v1,"v1");
		printSize(v1,"v1");
		std::cout << "Capacity vector v1:"<< v1.capacity() << std::endl;

		std::cout << "\n---------------reserve------------------"<<std::endl;
		//giving predifined capacity to vector
		v1.reserve(100);
		printVector(v1,"v1");
		printSize(v1,"v1");
		std::cout << "Capacity vector v1:"<< v1.capacity() << std::endl;
			
		std::cout << "\n---------------shrink_to_fit------------------"<<std::endl;
		//reduces capacity to size
		v1.shrink_to_fit();
		printVector(v1,"v1");
		printSize(v1,"v1");
		std::cout << "Capacity vector v1:"<< v1.capacity() << std::endl;



		std::cout << "\n---------------emplace------------------"<<std::endl;
		//inserts element at given position(by iterator) and increases new capacity if size ==capacity
		v.emplace(v.begin(),10000);
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		
		std::cout << "\n---------------emplace_back------------------"<<std::endl;
		//inserts element at back and increases capacity if size==capacity
		v.emplace_back(1111);
		printVector(v,"v");
		printSize(v,"v");
		std::cout << "Capacity vector v:"<< v.capacity() << std::endl;
		
			
			
		return 0;

	}

	
	void printVector(std::vector<int> v,std::string vName) {

		std::vector<int>::iterator itr;
		
		std::cout << "Vector "<< vName << ": ";

		int cnt = 0;

		for(itr = v.begin();itr!=v.end();itr++) {
			std::cout << *itr <<" ";
			cnt++;
		}

		if(!cnt)
			std::cout << "empty";
		
		std::cout << std::endl;
	}

	void printSize(std::vector<int> v,std::string vName) {
		std::cout << "Size " << vName+": "<<v.size() <<std::endl;
	}

	void printCapacity(std::vector<int> v,std::string vName) {

	}
	

