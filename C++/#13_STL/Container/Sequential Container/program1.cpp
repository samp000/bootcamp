
	#include<iostream>
	#include<list> 

	std::_List_iterator<int> operator+(std::_List_iterator<int> itr,int i) {
		
		while(i) {
			itr++;
			i--;
		}
		return itr;
	}

	int main() {
	
		std::list<int> lst;
		std::list<int>::iterator itr;
		
		lst.push_back(10);
		lst.push_back(20);
		lst.push_back(30);
		lst.push_back(40);
		lst.push_back(50);

		for(itr=lst.begin();itr!=lst.end();itr++)
			std::cout << *itr << std::endl;

		itr = lst.begin();

		std::cout << "TEST: " << *(lst.begin()+3) << std::endl;

		return 0;
	}
