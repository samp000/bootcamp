
	#include<iostream>
	#include<array>


	void printArray(std::array<int,5> arr,std::string name) {
	
		std::cout << "Array "<<name <<": ";

		std::array<int,5>::iterator itr;
		for(itr=arr.begin();itr!= arr.end();itr++){	
			std::cout << *itr << " ";
		}

		std::cout << std::endl;
	}

	int main() {

		std::array<int,5> arr;
		std::array<int,5>::iterator itr;
		arr.fill(10);	

		arr[1] = 20;
		arr[3] = 30;
		arr[4] = 40;

		printArray(arr,"arr");


		std::cout << "\n---------------accessing data------------------"<<std::endl;
		std::cout << " arr[0]: " <<arr[0] << std::endl;
		std::cout << "at(1): " << arr.at(1) << std::endl;
		std::cout << "front: " << arr.front() << std::endl;
		std::cout << "back: " << arr.back() << std::endl;

		//data() returns pointer of first element
		std::cout << "data()+4: " <<*( arr.data()+4) << std::endl;
	
		std::array<int,5> arr1 = {100,200,300,400,500};	
	//	std::array<int,10> arr2 = {100,200,300,400,500};	//for swaping we need both arrays of same size;
		printArray(arr1,"arr1");
		
		std::cout << "\n---------------swap------------------"<<std::endl;
		arr.swap(arr1);
		printArray(arr,"arr");
		printArray(arr1,"arr1");
		
		std::cout << "\n---------------array Comparison------------------"<<std::endl;
		arr.fill(10);
		arr1.fill(10);
		printArray(arr,"arr");
		printArray(arr1,"arr1");

		if(arr == arr1)
		std::cout << "both arrays are equal" << std::endl;


		return 0;
	}

	



