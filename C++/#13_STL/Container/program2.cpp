
	#include<iostream>
	#include<vector>

	class Player {
	
		int jersyNo;
		std::string name;

		public:

			Player(int no,std::string name) {
			
				this->jersyNo = no;
				this->name = name;
			}

			void getInfo() {
			
				std::cout <<"JNo: "<<jersyNo<<" Name: "<<name << std::endl;
			}
	};

	int main() {
	
		Player p1(7,"MSD");
		Player p2(18,"Virat");
		Player p3(45,"Rohit");

		std::vector<Player> vobj = {p1,p2,p3};

		for(auto itr = vobj.begin();itr< vobj.end();itr++)
			(*itr).getInfo();

		return 0;
	}

