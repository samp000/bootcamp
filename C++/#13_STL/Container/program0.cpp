
	#include<iostream>
	#include<vector>

	int main() {
	
		std::vector<int> vobj;

		vobj.push_back(10);
		vobj.push_back(20);
		vobj.push_back(30);
		vobj.push_back(40);
		vobj.push_back(50);

		std::vector<int>::iterator itr;

		for(itr = vobj.begin();itr< vobj.end();itr++)
			std::cout << *itr << std::endl;

		return 0;
	}

