
	//lambda expression is nothing bu a object of a function(i.e. its a class )

	#include<iostream>
	
	int main(){
	
		int a=100;
		std::string name = "Sandesh";

		//"without mutable we cannot edit variables in capture"
		auto add = [=](int x,int y) -> int {
	
			a++;	
			std::cout << a <<":" << name << std::endl;
			return x+y;
		};

		std::cout << add(10,20) << std::endl;

		return 0;
	}

