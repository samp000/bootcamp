
	//lambda expression is nothing bu a object of a function(i.e. its a class )

	#include<iostream>
	
	int main(){
	
		int a=100;
		std::string name = "Sandesh";

		//"&" is pass by reference

		auto add = [&](int x,int y)mutable -> int {
	
			a++;	
			std::cout << a <<":" << name << std::endl;
			return x+y;
		};

		std::cout << a  << std::endl;
		std::cout << add(10,20) << std::endl;
		std::cout << a  << std::endl;

		return 0;
	}

