
	//lambda expression is nothing bu a object of a function(i.e. its a class )

	#include<iostream>
	
	int main(){
	
		int a=100;
		std::string name = "Sandesh";

		// "=" in capture is to give access to labda expression to variables in global scope
		auto add = [=](int x,int y) -> int {
		
			std::cout << a <<":" << name << std::endl;
			return x+y;
		};

		std::cout << add(10,20) << std::endl;

		return 0;
	}

	/*
	 Internal representation of above code

	 class Anonymous  {
	 
	 	//"variables in capture"
	 	int a;
		std::string name;

		Anonymous(int a,std::string name) {
		
			this->a = a;
			this->name = name;
		}

		//"overloaded operator() with parameter, which are passed to lambda expression"
		//"return type is same as lambda expression"

		int operator()(int x,int y) {
		
			//"line written in lambda expression"
			std::cout << this->a << ":" << this->name << std::endl;
			return x+y;
		}

	 }


	 * /

	
