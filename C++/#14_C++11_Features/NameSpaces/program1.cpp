
	#include<iostream>

	namespace Company1 {
	
		int empCount=1000;

		void compInfo() {
		
			std::cout << "Nvidia" << std::endl;
			std::cout << empCount << std::endl;
		}
	}
	
	namespace Company2 {
	
		int empCount=2000;

		void compInfo() {
		
			std::cout << "AMD" << std::endl;
			std::cout << empCount << std::endl;
		}
	}

	int main() {
	
		Company1::compInfo();
		Company2::compInfo();

		return 0;
	}
