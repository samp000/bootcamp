
	#include<iostream>

	class Demo {

		public:

			int x=10;
			int y=20;

		Demo() {
		
			std::cout<<"In no-arg constructor"<<std::endl;
			
		}

		Demo(int x) {
		
			std::cout<<"In arg constructor"<<std::endl;
			
		}

		Demo(Demo &obj){
		
			std::cout<<"In copy constructor"<<std::endl;
		}

		~Demo() {

			std::cout<<"In destructor"<<std::endl;
		}

	};

	int main() {
	
		Demo obj;
		Demo obj1 = obj;

		std::cout << obj.x << " " << obj.y << std::endl;
	
		obj.x  = 100;
		obj.y  = 200;

		std::cout << obj.x << " " << obj.y << std::endl;
		std::cout << obj1.x << " " << obj1.y << std::endl;

		return 0;
	}
