
	#include<iostream>

	int main() {
	
		int x = 10;
		int z = 20;
		int &y = x;
		int *ptr = &x;

		std::cout << x << std::endl;
		std::cout << y << std::endl;
		std::cout << ptr << std::endl;
		
		std::cout << &x << std::endl;
		std::cout << &y << std::endl;
		std::cout << &ptr << std::endl;

		y = 20;
		
		std::cout << x << std::endl;
		std::cout << y << std::endl;


		return 0;
	}
