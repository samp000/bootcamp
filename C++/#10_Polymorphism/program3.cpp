
	//primitie and covarient return type


	#include<iostream>

	class Parent {
	
		public:

			virtual Parent* getData() {
			
				std::cout << "Parent getData()" <<std::endl;
				return this;
			}
	};

	class Child:public Parent {
	
		public:

			Parent* getData() {		//virtual implicitly
			
				std::cout << "Child getData()" <<std::endl;
				return this;
			}
			
	};

	int main() {
	
		Parent *obj = new Child();
		Parent* p = obj -> getData();
		std::cout << p << std::endl;

		return 0;
	}
