
	#include<iostream>

	class Parent {

		public:	
		virtual void marry() = 0; //pure virtual function

		virtual void property(){
		
			std::cout << "Flat Car, gold" << std::endl;
		}
	};

	class Child:public Parent {
	
		public:
			void marry() {
			
				std::cout << "XYZ" << std::endl;
			}
	};
	

	int main() {
	
		Parent *obj = new Child();
		obj->marry();

	}
