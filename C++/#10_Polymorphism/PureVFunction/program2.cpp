
	#include<iostream>

	/*	
	 *	if class has all pure virtual functions then make write class name with prefixed "I"
	 *	its not a compultion but its a convention
	 *
	 *	this class is acts as a Interface if all methods are pure virtual
	 *	else if partial methods are pure virtual then that class acts as a abstract class
	 */

	class IParent {

		public:	
		virtual void marry() = 0; //pure virtual function

		virtual void property()=0;
	};

	class Child:public IParent {
	
		public:
			void marry() {
			
				std::cout << "XYZ" << std::endl;
			}

			void property() {
			
				std::cout << "Flat Car, gold" << std::endl;
			}
	};
	

	int main() {
		IParent *obj = new Child();
		obj->marry();
	}
