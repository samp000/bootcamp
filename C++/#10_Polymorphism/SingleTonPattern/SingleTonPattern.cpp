
	#include<iostream>

	class Demo {
	
		private:

			static Demo* obj;

			Demo() {
			
				std::cout << "Demo constructor" << std::endl;
			}

		public:
			static Demo* getInstance(){
			
				return obj;
			}

			void getData() {
			
				std::cout << "This:" << this<<std::endl;
			}

	};

	Demo* Demo::obj = new Demo();

	int main() {
	
		Demo* obj1 = Demo::getInstance();
		Demo* obj2 = Demo::getInstance();

		//SAME ADDRESS BECAUSE this returns same object
		std::cout << obj1 << std::endl;
		std::cout << obj2 << std::endl;

		obj1->getData();
		obj2->getData();

		return 0;
	}
