
	//constidentifier in c++

	#include<iostream>

	class Parent {		
	
		public:

			virtual void getData() const {
			
				std::cout << "Parent getData()" <<std::endl;
			}
	};

	class Child:public Parent {
	
		public:
			
			 void getData() const override{
			
				std::cout << "Child getData()" <<std::endl;
			}

			
	};

	int main() {
	
		Parent *obj = new Child();
		obj -> getData();

		//function which is const is called

		return 0;
	}
