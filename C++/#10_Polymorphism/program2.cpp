

	//method signature in overriding

	#include<iostream>

	class Parent{
	
		public:
			Parent() {
			
				std::cout << "Parent Constructor" << std::endl;
			}

			virtual void getData(int x){ // getData(int) 
			
				std::cout << "Parent getData"  << std::endl;
			}

			virtual void printData(float x) { //printData(float)
	
				std::cout << "Parent printData" << std::endl;
			}
	};

	class Child: public Parent {
	
		public:
			Child() {
			
				std::cout << "Child Constructor" << std::endl;
			}

			//overriding not allowd
			virtual void getData(short int x){ // getData(short int) 
			
				std::cout << "Child getData"  << std::endl;
			}

			//overriding
			virtual void printData(float x) { //printData(float)
				
				std::cout << "Child printData" << std::endl;
			}
			
	};

	int main() {
	
		Parent *obj = new Child();
		obj->getData(10);
		obj->printData(20.5);

		Child obj1;
		Parent *obj2 = &obj1;
		obj2->getData(10);
		obj2->printData(20.5);

		Child obj3;
		Parent& obj4 = obj3;
		obj4.getData(10);
		obj4.printData(25.5);


		return 0;
	}
