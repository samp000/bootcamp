
	//final identifier in c++

	#include<iostream>

	class Parent final{		//cannot inherit final class
	
		public:

			virtual void getData() {
			
				std::cout << "Parent getData()" <<std::endl;
			}
	};

	class Child:public Parent {
	
		public:

			void getData() final {		//method is final
			
				std::cout << "Child getData()" <<std::endl;
			}
			
	};

	class Child1:public Child {
	
		//error
		void getData() {
		
			
		}
	};

	int main() {
	
		Parent *obj = new Child();
		obj -> getData();

		return 0;
	}
