
	#include<iostream>

	class Parent {
	
		public:
			Parent(){
			
				std::cout << "Parent constructor" <<std::endl;
			}

			virtual void getData() {
			
				std::cout << "Parent getData()" <<std::endl;
			}
	};

	class Child:public Parent {
	
		public:
			Child(){
			
				std::cout << "Child constructor" << std::endl;
			}

			void getData() {		//virtual implicitly
			
				std::cout << "Child getData()" <<std::endl;
			}
			
	};

	int main() {
	
		Parent obj1;
		obj1.getData();

		Child obj2;
		obj2.getData();

		Parent *obj3 = new Child();
		obj3->getData();
		return 0;
	}
