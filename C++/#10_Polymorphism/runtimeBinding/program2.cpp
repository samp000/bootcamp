
	#include<iostream>
	#pragma pack(4)

	class Parent {


		public:
			virtual void getData() {
			
				std::cout << "Parent GetData" << std::endl;
			}

			virtual void printData() {
				
				std::cout << "Parent PrintData" << std::endl;
			}
	};	

	class Child1:public Parent {
	
		public:

			void getData() {
			
				std::cout << "Child1 GetData" <<std::endl;
			}
	};

	class Child2:public Parent {

		public:

			void printData(){
			
				std::cout <<"Child2 printData" <<std::endl;
			}
	};

	int main() {
	
		/*	there is no any variables in Parent class but it still shows 8 byte as a size
		 *	this is because when we make any virtual function in class it generates a "_vptr"
		 *	pointer which points to a "vTable" which is "array of a function pointer"
		 *
		 */
		std::cout << sizeof(Parent) << std::endl;	

		Parent *obj1 = new Child1();
		obj1->getData();

		/*
		 * 	When there is a call to a virtual function then that function is not called on object 
		 * 	but it is called on "_vptr"(for every class vtable is differnt as "_vPtr" also) 
		 *
		 * 	like:
		 *
		 * 	obj1 -> _vptr -> getData();
		 *
		 *
		 * */

		/*
		 * 	By these we can observe that _vptr is stored in base address of class..
		 * 	because if we increment that pointer by one then itr will plaus by the no of bytes 
		 * 	of size of the class in our case there is only 8 bytes (these is by _ptr pointer which is
		 * 	creaed by compiler it self)
		 *
		 */
		std::cout << obj1 << std::endl;
		std::cout << obj1+1 << std::endl;


		/*
		
		uintptr_t* vptr = reinterpret_cast<uintptr_t*>(obj1);
		uintptr_t vtablePtr = *vptr;

		std::cout << vptr << std::endl;
		std::cout << vtablePtr << std::endl;
		
		*/
	}
