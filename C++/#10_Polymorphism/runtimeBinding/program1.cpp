
	//late Binding

	#include<iostream>

	void add(int x,int y) {
		std::cout << x+y << std::endl;
	}
	void sub(int x,int y) {
		std::cout << x-y << std::endl;
	}
	void mul(int x,int y) {
		std::cout << x*y << std::endl;
	}

	int main() {

		std::cout << "1.Add" << std::endl;	
		std::cout << "2.Sub" << std::endl;	
		std::cout << "3.Mul" << std::endl;	

		int ch;

		std::cout << "Enter choice:" << std::endl;
		std::cin >> ch;

		void (*funPtr)(int,int) = NULL;

		switch(ch) {
		
			case 1:
				funPtr = add;
				break;
			case 2:
				funPtr = sub;
				break;
			case 3:
				funPtr = mul;
				break;
			default:
				std::cout << "Incorrect choice" <<std::endl;
		}

		funPtr(10,20);

		return 0;
	}

/*	In this program which method is to be called is decided at runtime
 *	
 */
