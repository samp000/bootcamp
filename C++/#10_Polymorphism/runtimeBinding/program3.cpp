
	//This program demonstrates vtable and vptr

	#include<iostream>

	void add(int x,int y) {
		std::cout << "Add: "<< x+y << std::endl;
	}
	
	void sub(int x,int y) {
		std::cout << "Sub:" << x-y << std::endl;
	}

	void mul(int x,int y) {
		std::cout << "Mul: " << x*y << std::endl;
	}

	int main() {
	
		void (*fPtr[3]) (int,int);


		fPtr[0] = add;
		fPtr[1] = sub;
		fPtr[2] = mul;

		void (*_vptr)(int,int) = fPtr[0];

		_vptr(10,20);
		
		return 0;

	}
