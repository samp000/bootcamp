
	//class Parent

	#include<iostream>

	class Parent {
	
		public:
			Parent() {
				std::cout << "Parent constructor" << std::endl;
			}
	};

	class Child1:public Parent {
	
		public:
			Child1() {
				std::cout << "Child1 constructor" << std::endl;
			}
	};

	class Child2:public Parent {
	
		public:
			Child2() {
				std::cout << "Child2 constructor" << std::endl;
			}
	};

	class Child:public Child1,public Child2 {
	
		public:
			Child() {
				std::cout << "Child constructor" << std::endl;
			}
	};

	int main() {
	
		Child obj;

		return 0;
	}
