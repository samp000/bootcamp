
	//virtual base class does construction only once... if we still uses diamond inheritance
	//proof is this program

	#include<iostream>

	class Parent {
	
		public:
			Parent() {
				std::cout << "Parent constructor" << std::endl;
			}

			void getData() {
			
				std::cout << "Parent GetData()" << std::endl;
			}
	};

	class Child1:virtual public Parent {
	
		public:
			Child1() {
				std::cout << "Child1 constructor" << std::endl;
			}
	};

	class Child2:virtual public Parent {
	
		public:
			Child2() {
				std::cout << "Child2 constructor" << std::endl;
			}
	};

	class Child:public Child1,public Child2 {
	
		public:
			Child() {
				std::cout << "Child constructor" << std::endl;
			}
	};

	int main() {
	
		Child obj;
		obj.getData();	//ambiguity
		return 0;
	}
