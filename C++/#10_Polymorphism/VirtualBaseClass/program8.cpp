
	/*
	 * 	this program is proof that in virtual base class.. last child in diamond inheritance will get
	 * 	only top parents content.... just remove virtual of top parent and give to child1 or child2
	 * 	still it will inherit top parents getData();
	 * 	for stat solun is make methods in Parent as virtual
	 *
	 * 					Parent
	 *					/ | \
	 *				       /  |  \
	 *				      /   |   \
	 *				  Child 1 | Child 2
	 *				    \	  |    /
	 *				     \	  |   /
	 *				      \	  |  /
	 *				       \  | /
	 *					Child
	 *
	 *
	 */
	

	#include<iostream>

	class Parent {
	
		public:

			 virtual void getData() {
				std::cout << "Parent GetData()" << std::endl;
			}
	};

	//virtual base class
	class Child1: virtual public Parent {
			 
		void getData() {
			std::cout << "Child1 GetData()" << std::endl;
		}
	};

	//virtual base class
	class Child2:virtual public Parent {
			
		void getData() {
			std::cout << "Child2 GetData()" << std::endl;
		}
	
	};

	class Child:public Child1,public Child2 {
	
		void getData() {
			std::cout << "Child GetData()" << std::endl;
		}
	};

	int main() {
	
		Parent *obj = new Child();
		obj->getData();
		return 0;
	}
