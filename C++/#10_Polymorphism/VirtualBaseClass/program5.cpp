
	//virtual destructor

	#include<iostream>
	class Parent {

		public:

		Parent() {
			std::cout << "Parent constructor" << std::endl;
		}
		
		~Parent() {
			std::cout << "Parent destructor" << std::endl;
		}
	};

	class Child: public Parent{
	
		int *ptr = NULL;

		public:

			Child() {
				ptr= new int[5];
				std::cout << "Child constructor" << std::endl;
			}
		
			~Child() {
				std::cout << "Child destructor" << std::endl;
				delete[] ptr;
			}
	};

	int main(){
	
		/*	this causes call to constructors in sequence Parent -> Child
		 *	and after all calls call to destructors in sequence Child -> Parent
		 *
		 */
		Child obj;

		return 0;
	}
