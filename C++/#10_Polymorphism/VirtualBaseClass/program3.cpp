
	

	#include<iostream>

	class Parent {
	
		public:

			 void getData() {
			
				std::cout << "Parent GetData()" << std::endl;
			}
	};

	//virtual base class
	class Child1: virtual public Parent {
	
	};

	//virtual base class
	class Child2:virtual public Parent {
	
	};

	class Child:public Child1,public Child2 {
	
	};

	int main() {
	
		Child obj;
		obj.getData();	//ambiguity
		return 0;
	}
