
	//override identifier in c++

	#include<iostream>

	class Parent {		
	
		public:

			virtual void getData() {
			
				std::cout << "Parent getData()" <<std::endl;
			}
	};

	class Child:public Parent {
	
		public:
			
			 void getData() {
			
				std::cout << "Parent getData()" <<std::endl;
			}

			void getData(int x) override {//checks if parent method is overrided or not		
			
				std::cout << "Child getData()" <<std::endl;
			}
			
	};

	int main() {
	
		Parent *obj = new Child();
		obj -> getData();

		return 0;
	}
