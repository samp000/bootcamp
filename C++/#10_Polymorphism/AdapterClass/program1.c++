
	#include<iostream>

	class IParent {
	
		public:
			virtual void fun1()=0;
			virtual void fun2()=0;

	};

	//this is just a dummy class
	class Adapter:public IParent {
	
		void fun1(){}
		void fun2(){}
	};

	class Child1:public Adapter {
	
		void fun1(){
			std::cout << "Fun1 child1" <<std::endl;
		}
	};
	
	class Child2:public Adapter {
	
		void fun2(){
			std::cout << "Fun2 child1" <<std::endl;
		}
	};

	int main() {
	
		IParent *obj1 = new Child1();
		obj1->fun1();

		return 0;
	}
