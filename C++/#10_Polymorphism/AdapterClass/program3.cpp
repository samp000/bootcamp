
	//static in overriding

	#include<iostream>

	class Demo {
	
		public:
			//virtual static void getData()		error
			static void getData() {
				std::cout << "parent getdata" << std::endl;
			}

	};

	class DemoChild:public Demo {
	
		public:
			static void getData() {
				std::cout << "child getdata" << std::endl;
			}

	};

	int main() {
	
		Demo *obj = new DemoChild();
		obj->getData();

		return 0;
	}

	/*
	 * 	When we make both overriding and overriden methods static then its not a scenario of polymorphism
	 * 	That time method will be called on the basis of reference on which it is called
	 *
	 */
